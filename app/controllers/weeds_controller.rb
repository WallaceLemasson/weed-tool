class WeedsController < ApplicationController
	before_action :find_weed, only: [:show, :edit, :update, :destroy, :upvote, :downvote]
	before_action :authenticate_user!, except: [:index, :show]

	def index
		if params[:category].blank?
			@weeds = Weed.all.order("created_at DESC")
		else
			@category_id = Category.find_by(name: params[:category]).id
			@weeds = Weed.where(category_id: @category_id).order("created_at DESC")
		end
	 
	end

	def show
	end

	def new
		@weed = current_user.weeds.build
	end

	def create
		@weed = current_user.weeds.build(weed_params)

		if @weed.save
			redirect_to @weed
		else
			render "new"
		end
	end

	def edit
	end

	def update
		if @weed.update(weed_params)
			redirect_to @weed
		else
			render "edit"
		end
	end

	def destroy
		@weed.destroy
		redirect_to root_path
	end

	def upvote
		@weed.upvote_by current_user
		redirect_to :back
	end

	def downvote
		@weed.downvote_by current_user
		redirect_to :back
	end	

	private

	def find_weed
		@weed = Weed.find(params[:id])
	end

	def weed_params
		params.require(:weed).permit(:title, :comment, :user_id, :image, :category_id)
	end
end
