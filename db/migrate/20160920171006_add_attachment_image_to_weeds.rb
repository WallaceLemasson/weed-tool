class AddAttachmentImageToWeeds < ActiveRecord::Migration
  def self.up
    change_table :weeds do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :weeds, :image
  end
end
