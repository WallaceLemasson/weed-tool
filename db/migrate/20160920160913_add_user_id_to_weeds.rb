class AddUserIdToWeeds < ActiveRecord::Migration
  def change
    add_column :weeds, :user_id, :integer
  end
end
