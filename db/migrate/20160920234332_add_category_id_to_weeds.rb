class AddCategoryIdToWeeds < ActiveRecord::Migration
  def change
    add_column :weeds, :category_id, :integer
  end
end
