# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create(email: 'yv@yv.com', password: '12345678')

Category.create(name: 'Sativa')
Category.create(name: 'Indica')

Weed.create(title: 'AK-47', category_id: 1, comment: 'Like that shit', user_id: 1)
Weed.create(title: 'Moby Dick', category_id: 2, comment: 'Like that shit', user_id: 1)
Weed.create(title: 'White Widow', category_id: 1, comment: 'Like that shit', user_id: 1)
Weed.create(title: 'California Cream', category_id: 1, comment: 'Like that shit', user_id: 1)
Weed.create(title: 'Super Skunk', category_id: 2, comment: 'Like that shit', user_id: 1)
Weed.create(title: 'Chunky Monkey', category_id: 1, comment: 'Like that shit', user_id: 1)
Weed.create(title: 'Cali Blue', category_id: 2, comment: 'Like that shit', user_id: 1)
Weed.create(title: 'Michael Jackson', category_id: 2, comment: 'Like that shit', user_id: 1)